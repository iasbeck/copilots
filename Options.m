%#ok<*CPROP>
%#ok<*MANU>
classdef Options < handle
    
    properties % ----------------------------------------------- properties
        algorithm;
        constraintTolerance;
        diagnostics;
        displayInfo;
        maxFunctionEvaluations;
        maxIterations;
        optimalityTolerance;
        functionTolerance;
        stepTolerance;
        maxTime;
        plotResults;
        plotTraj;
        plotTrajMidpoints;
        collocationType
    end
    
    methods % ----------------------------------------------------- methods
        
        function readInput(self) % ------------------------------ readInput
            % Carregamento do arquivo em que as variáveis estão definidas
            options;
            
            % Verificação das entradas do usuário
            inputCheckOptions;
            
            % Armazenamento das entradas do usuário
            self.algorithm = algorithm;
            self.constraintTolerance = constraintTolerance;
            self.diagnostics = diagnostics;
            self.displayInfo = displayInfo;
            self.maxFunctionEvaluations = maxFunctionEvaluations;
            self.maxIterations = maxIterations;
            self.optimalityTolerance = optimalityTolerance;
            self.functionTolerance = functionTolerance;
            self.stepTolerance = stepTolerance;
            self.plotResults = plotResults;
            self.plotTraj = plotTraj;
            self.plotTrajMidpoints = plotTrajMidpoints;
            self.collocationType = collocationType;
            self.maxTime = maxTime;
        end
        
        function opt = getFminOptions(self) % -------------- getFminOptions
            opt = optimoptions(@fmincon, ...
                'Algorithm', self.algorithm, ...
                'ConstraintTolerance', self.constraintTolerance, ...
                'Diagnostics', self.diagnostics, ...
                'Display', self.displayInfo, ...
                'MaxFunctionEvaluations', self.maxFunctionEvaluations, ...
                'MaxIterations', self.maxIterations, ...
                'OptimalityTolerance', self.optimalityTolerance, ...
                'FunctionTolerance', self.functionTolerance, ...
                'StepTolerance', self.stepTolerance);
        end
    end
end

