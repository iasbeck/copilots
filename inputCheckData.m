% Variável booleana para indicação de um erro
inputError = 0;

%% Verificação da declaração das variáveis básicas
% Definição de nx, nu e N
if ~exist('nu', 'var') || ~exist('nx', 'var') || ...
        ~exist('N', 'var')
    fprintf('N, nu e nx devem ser obrigatoriamente ');
    fprintf('informados! \n\n');
    error('Erro nas entradas do usuário...');
end

if nu <= 0 || nx <= 0 || N <= 0
    fprintf('N, nx e nu devem ser maiores do que zeros! \n\n');
    error('Erro nas entradas do usuário...');
end

%% Verificação das dimensões das variáveis
% Dimensão de t0L
if exist('t0L', 'var') && (size(t0L,1) ~= 1 || size(t0L,2) ~= 1)
    fprintf('t0L deve ser um número e não um vetor!');
    inputError = 1;
end

% Dimensão de t0U
if exist('t0U', 'var') && (size(t0U,1) ~= 1 || size(t0U,2) ~= 1)
    fprintf('t0U deve ser um número e não um vetor!');
    inputError = 1;
end

% Dimensão de tfL
if exist('tfL', 'var') && (size(tfL,1) ~= 1 || size(tfL,2) ~= 1)
    fprintf('tfL deve ser um número e não um vetor!');
    inputError = 1;
end

% Dimensão de tfU
if exist('tfU', 'var') && (size(tfU,1) ~= 1 || size(tfU,2) ~= 1)
    fprintf('tfU deve ser um número e não um vetor!');
    inputError = 1;
end

% Dimensão de uL
if exist('uL', 'var')
    if ~iscolumn(uL); uL = uL'; end
    if size(uL,2) > 1
        fprintf('O vetor uL deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(uL) ~= nu
        fprintf('Foi informado que nu = %d, porém o vetor uL tem ', nu);
        fprintf('%d elemento(s)! \n', length(uL));
        inputError = 1; 
    end
end

% Dimensão de uU
if exist('uU', 'var')
    if ~iscolumn(uU); uU = uU'; end
    if size(uU,2) > 1
        fprintf('O vetor uU deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(uU) ~= nu
        fprintf('Foi informado que nu = %d, porém o vetor uL tem ', nu);
        fprintf('%d elemento(s)! \n', length(uU));
        inputError = 1; 
    end
end

% Dimensão de xL
if exist('xL', 'var')
    if ~iscolumn(xL); xL = xL'; end
    if size(xL,2) > 1
        fprintf('O vetor xL deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(xL) ~= nx
        fprintf('Foi informado que nx = %d, porém o vetor xL tem ', nx);
        fprintf('%d elemento(s)! \n', length(xL));
        inputError = 1; 
    end
end

% Dimensão de xU
if exist('xU', 'var')
    if ~iscolumn(xU); xU = xU'; end
    if size(xU,2) > 1
        fprintf('O vetor xU deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(xU) ~= nx
        fprintf('Foi informado que nx = %d, porém o vetor xU tem ', nx);
        fprintf('%d elemento(s)! \n', length(xU));
        inputError = 1; 
    end
end

% Dimensão de x0L
if exist('x0L', 'var')
    if ~iscolumn(x0L); x0L = x0L'; end
    if size(x0L,2) > 1
        fprintf('O vetor x0L deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(x0L) ~= nx
        fprintf('Foi informado que nx = %d, porém o vetor x0L tem ', nx);
        fprintf('%d elemento(s)! \n', length(x0L));
        inputError = 1; 
    end
end

% Dimensão de x0U
if exist('x0U', 'var')
    if ~iscolumn(x0U); x0U = x0U'; end
    if size(x0U,2) > 1
        fprintf('O vetor x0U deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(x0U) ~= nx
        fprintf('Foi informado que nx = %d, porém o vetor x0U tem ', nx);
        fprintf('%d elemento(s)! \n', length(x0U));
        inputError = 1; 
    end
end

% Dimensão de xfL
if exist('xfL', 'var')
    if ~iscolumn(xfL); xfL = xfL'; end
    if size(xfL,2) > 1
        fprintf('O vetor xfL deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(xfL) ~= nx
        fprintf('Foi informado que nx = %d, porém o vetor xfL tem ', nx);
        fprintf('%d elemento(s)! \n', length(xfL));
        inputError = 1; 
    end
end

% Dimensão de xfU
if exist('xfU', 'var')
    if ~iscolumn(xfU); xfU = xfU'; end
    if size(xfU,2) > 1
        fprintf('O vetor xfU deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(xfU) ~= nx
        fprintf('Foi informado que nx = %d, porém o vetor xfU tem ', nx);
        fprintf('%d elemento(s)! \n', length(xfU));
        inputError = 1; 
    end
end

% Dimensão de xNames
if exist('xNames', 'var')
    if ~iscolumn(xNames); xNames = xNames'; end
    if size(xNames,2) > 1
        fprintf('A célula xNames deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(xNames) ~= nx
        fprintf('Foi informado que nx = %d, porém o vetor xNames tem ', nx);
        fprintf('%d elemento(s)! \n', length(xNames));
        inputError = 1; 
    end
end

% Dimensão de uNames
if exist('uNames', 'var')
    if ~iscolumn(uNames); uNames = uNames'; end
    if size(uNames,2) > 1
        fprintf('A célula uNames deve ter somente uma linha ou uma coluna! \n');
        inputError = 1; 
    elseif length(uNames) ~= nu
        fprintf('Foi informado que nu = %d, porém o vetor uNames tem ', nu);
        fprintf('%d elemento(s)! \n', length(uNames));
        inputError = 1; 
    end
end

% Dimensões de X0
if exist('X0', 'var')
    if size(X0, 1) ~= nx || size(X0, 2) ~= N
        fprintf('O número de linhas de X0 deve ser igual a nx e o número ');
        fprintf('de colunas igual a N! \n');
        inputError = 1; 
    end
end

% Dimensões de X0
if exist('U0', 'var')
    if size(U0, 1) ~= nu || size(U0, 2) ~= N
        fprintf('O número de linhas de U0 deve ser igual a nu e o número ');
        fprintf('de colunas igual a N! \n');
        inputError = 1; 
    end
end

% Dimensões de X0
if exist('T0', 'var')
    if ~iscolumn(T0); T0 = T0'; end
    if size(T0, 1) ~= 2 || size(T0, 2) ~= 1
        fprintf('O vetor T0 deve conter uma única linha ou uma única ');
        fprintf('coluna com apenas dois elementos!');
        inputError = 1; 
    end
end

%% Verificação das declarações das variáveis
% Declaração de t0L e t0U
if ~exist('t0L', 'var') && ~exist('t0U', 'var')
    if exist('t0', 'var')
        t0L = t0;
        t0U = t0;
    else
        fprintf('t0L e t0U não foram informados! Adotou-se ');
        fprintf('t0L = 0 e t0U = 0! \n');
        t0L = 0;
        t0U = 0;
    end
elseif ~exist('t0L', 'var') && exist('t0U', 'var')
    fprintf('t0U foi declarado mas t0L não! \n');
    inputError = 1;
elseif exist('t0L', 'var') && ~exist('t0U', 'var')
    fprintf('t0L foi declarado mas t0U não! \n');
    inputError = 1;
end

% Declaração de tfL e tfU
if ~exist('tfL', 'var') && ~exist('tfU', 'var')
    if exist('tF', 'var')
        tfL = tF;
        tfU = tF;
    else
        fprintf('tfL e tfU não foram informados! Adotou-se ');
        fprintf('tfL = 1 e tfU = 1! \n');
        tfL = 1;
        tfU = 1;
    end
elseif ~exist('tfL', 'var') && exist('tfU', 'var')
    fprintf('tfU foi declarado mas tfL não! \n');
    inputError = 1;
elseif exist('tfL', 'var') && ~exist('tfU', 'var')
    fprintf('tfL foi declarado mas tfU não! \n');
    inputError = 1;
end

% Definição de uL e uU
if ~exist('uL', 'var'); uL = -inf*ones(nu, 1); end
if ~exist('uU', 'var'); uU = inf*ones(nu, 1); end

% Definição de xL e xU
if ~exist('xL', 'var'); xL = -inf*ones(nx, 1); end
if ~exist('xU', 'var'); xU = inf*ones(nx, 1); end

% Definição de x0
if ~exist('x0L', 'var') && ~exist('x0U', 'var')
    if exist('x0', 'var')
        x0L = x0;
        x0U = x0;
    else
        fprintf('É necessário informar os limites superiores ');
        fprintf('e inferiores para x0, ou o valor de x0. \n');
        fprintf('Nenhum destes foi informado! \n');
        inputError = 1;
    end
elseif ~exist('x0L', 'var') && exist('x0U', 'var')
    fprintf('x0U foi declarado mas x0L não! \n');
    inputError = 1;
elseif exist('x0L', 'var') && ~exist('x0U', 'var')
    fprintf('x0L foi declarado mas x0U não! \n');
    inputError = 1;
end

% Definição de xf
if ~exist('xfL', 'var') && ~exist('xfU', 'var')
    if exist('xf', 'var')
        xfL = xf;
        xfU = xf;
    else
        fprintf('É necessário informar os limites superiores ');
        fprintf('e inferiores para xf, ou o valor de xf. \n');
        fprintf('Nenhum destes foi informado! \n');
        inputError = 1;
    end
elseif ~exist('xfL', 'var') && exist('xfU', 'var')
    fprintf('xfU foi declarado mas xfL não! \n');
    inputError = 1;
elseif exist('xfL', 'var') && ~exist('xfU', 'var')
    fprintf('xfL foi declarado mas xfU não! \n');
    inputError = 1;
end

% Definição dos nomes dos estados
if ~exist('xNames', 'var')
    if nx == 1
        uNames = {'x'};
    else
        xNames = cell(1, nx);
        for i = 1:nx
            xNames{i} = strcat('x', num2str(i));
        end
    end
end

% Definição dos nomes dos controles
if ~exist('uNames', 'var')
    if nu == 1
        uNames = {'u'};
    else
        uNames = cell(1, nu);
        for i = 1:nu
            uNames{i} = strcat('u', num2str(i));
        end
    end
end

%% Definição dos palpites iniciais
% Definição de X0
if ~exist('X0', 'var')
    X0 = zeros(nx, N);
    for i = 1:nx
        if ~isinf(xL(i)) && ~isinf(xU(i))
            X0(i, :) = ((xU(i) + xL(i))/2)*ones(1, N);
        elseif isinf(xL(i)) && ~isinf(xU(i))
            X0(i, :) = xU(i)*ones(1, N);
        elseif ~isinf(xL(i)) && isinf(xU(i))
            X0(i, :) = xL(i)*ones(1, N);
        end
    end
end

% Definição de X0
if ~exist('U0', 'var')
    U0 = zeros(nu, N);
    for i = 1:nu
        if ~isinf(uL(i)) && ~isinf(uU(i))
            U0(i, :) = ((uU(i) + uL(i))/2)*ones(1, N);
        elseif isinf(uL(i)) && ~isinf(uU(i))
            U0(i, :) = uU(i)*ones(1, N);
        elseif ~isinf(uL(i)) && isinf(uU(i))
            U0(i, :) = uL(i)*ones(1, N);
        end
    end
end

% Definição de T0
if ~exist('T0', 'var')
    T0 = [0; 0];
    % Tempo inicial
    if ~isinf(t0U)
        T0(1) = (t0L + t0U)/2;
    elseif isinf(t0U)
        T0(1) = t0L;
    end
    
    % Tempo final
    if ~isinf(tfU)
        T0(2) = (tfL + tfU)/2;
    elseif isinf(t0U)
        T0(2) = tfL;
    end
end

% Apresentação de um erro ao usuário e encerramento da execução
if inputError
    fprintf('\n')
    error('Erro nas entradas do usuário...');
end