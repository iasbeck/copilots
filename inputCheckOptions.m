% Variável booleana para indicação de um erro
inputError = 0;

%% Verificação de variáveis informadas pelo usuário
if exist('algorithm', 'var')
    if ~strcmp(algorithm, 'sqp') && ~strcmp(algorithm, 'interior-point') ...
            && ~strcmp(algorithm, 'active-set') && ~strcmp(algorithm, 'sqp-legacy')
        fprintf('À variável ''algorithm'' deve-se atribuir ''sqp'', ');
        fprintf('''interior-point'', ''active-set'' ou ''sqp-legacy''!');
        fprintf(' ''%s'' não está disponível! \n', algorithm);
        inputError = 1; 
    end
end

if exist('diagnostics', 'var')
    if ~strcmp(diagnostics, 'on') && ~strcmp(diagnostics, 'off')
        fprintf('À variável ''diagnostics'' deve-se atribuir ''on'' ');
        fprintf('ou ''off''!');
        fprintf(' ''%s'' não está disponível! \n', diagnostics);
        inputError = 1; 
    end
end

if exist('displayInfo', 'var')
    if ~strcmp(displayInfo, 'iter') && ~strcmp(displayInfo, 'iter-detailed')
        fprintf('À variável ''displayInfo'' deve-se atribuir ''iter'' ');
        fprintf('ou ''iter-detailed''!');
        fprintf(' ''%s'' não está disponível! \n', displayInfo);
        inputError = 1; 
    end
end

if exist('maxFunctionEvaluations', 'var')
    if size(maxFunctionEvaluations, 1) ~= 1 || size(maxFunctionEvaluations, 2) ~= 1
        fprintf('''maxFunctionEvaluations'' deve ser um número, e não um vetor! \n');
        inputError = 1; 
    end
end

if exist('maxIterations', 'var')
    if size(maxIterations, 1) ~= 1 || size(maxIterations, 2) ~= 1
        fprintf('''maxIterations'' deve ser um número, e não um vetor! \n');
        inputError = 1; 
    end
end

if exist('optimalityTolerance', 'var')
    if size(optimalityTolerance, 1) ~= 1 || size(optimalityTolerance, 2) ~= 1
        fprintf('''optimalityTolerance'' deve ser um número, e não um vetor! \n');
        inputError = 1; 
    end
end

if exist('constraintTolerance', 'var')
    if size(constraintTolerance, 1) ~= 1 || size(constraintTolerance, 2) ~= 1
        fprintf('''constraintTolerance'' deve ser um número, e não um vetor! \n');
        inputError = 1; 
    end
end

if exist('functionTolerance', 'var')
    if size(functionTolerance, 1) ~= 1 || size(functionTolerance, 2) ~= 1
        fprintf('''functionTolerance'' deve ser um número, e não um vetor! \n');
        inputError = 1; 
    end
end

if exist('maxTime', 'var')
    if size(maxTime, 1) ~= 1 || size(maxTime, 2) ~= 1
        fprintf('''maxTime'' deve ser um número, e não um vetor! \n');
        inputError = 1; 
    end
end

if exist('stepTolerance', 'var')
    if size(stepTolerance, 1) ~= 1 || size(stepTolerance, 2) ~= 1
        fprintf('''stepTolerance'' deve ser um número, e não um vetor! \n');
        inputError = 1; 
    end
end

if exist('plotResults', 'var')
    if plotResults ~= 1 && plotResults ~= 0
        fprintf('''plotResults'' deve assumir um valor igual a 0 ou a 1! ');
        fprintf('''%d'' não é permitido! \n', plotResults);
        inputError = 1; 
    end
end

if exist('plotTraj', 'var')
    if plotResults ~= 1 && plotResults ~= 0
        fprintf('''plotTraj'' deve assumir um valor igual a 0 ou a 1! ');
        fprintf('''%d'' não é permitido! \n', plotTraj);
        inputError = 1; 
    end
end

if exist('plotTrajMidpoints', 'var')
    if plotTrajMidpoints ~= 1 && plotTrajMidpoints ~= 0
        fprintf('''plotTrajMidpoints'' deve assumir um valor igual a 0 ou a 1! ');
        fprintf('''%d'' não é permitido! \n', plotTrajMidpoints);
        inputError = 1; 
    end
end

if exist('collocationType', 'var')
    if ~strcmp(collocationType, 'trap') && ~strcmp(collocationType, 'hersim') 
        fprintf('À variável ''collocationType'' deve-se atribuir ''trap'' ');
        fprintf('ou ''hersim''!');
        fprintf(' ''%s'' não está disponível! \n', collocationType);
        inputError = 1; 
    end
end

%% Declaração de variáveis não informadas pelo usuário 
if ~exist('algorithm', 'var')
    algorithm = 'sqp';
end

if ~exist('diagnostics', 'var')
    diagnostics = 'off';
end

if ~exist('displayInfo', 'var')
    displayInfo = 'iter';
end

if ~exist('maxFunctionEvaluations', 'var')
    maxFunctionEvaluations = inf;
end

if ~exist('maxIterations', 'var')
    maxIterations = 1000;
end

if ~exist('optimalityTolerance', 'var')
    optimalityTolerance = 1e-6;
end

if ~exist('constraintTolerance', 'var')
    constraintTolerance = 1e-6;
end

if ~exist('functionTolerance', 'var')
    functionTolerance = 1e-6;
end

if ~exist('stepTolerance', 'var')
    stepTolerance = 1e-6;
end

if ~exist('maxTime', 'var')
    maxTime = inf;
end

if ~exist('plotResults', 'var')
    plotResults = 1;
end

if ~exist('plotTraj', 'var')
    plotTraj = 0;
end

if ~exist('plotTrajMidpoints', 'var')
    plotTrajMidpoints = 0;
end

if ~exist('collocationType', 'var')
    collocationType = 'trap';
end

if inputError
    fprintf('\n');
    error('Erro nas entradas ');
end