%#ok<*MANU> 
%#ok<*CPROP>
%#ok<*PROP>
%#ok<*NODEF>
classdef Data < handle
    
    properties % ----------------------------------------------- properties
        nx
        nu
        N
        Nt
        t0L
        t0U
        tfL
        tfU
        uL
        uU
        xL
        xU
        x0L
        x0U
        xfL
        xfU
        U0
        X0
        T0
        xNames
        uNames
    end
    
    methods % ----------------------------------------------------- methods
        
        function readInput(self) % ------------------------------ readInput
            % Carregamento do arquivo em que as variáveis estão definidas
            config;
            
            % Verificação das entradas do usuário
            inputCheckData;
            
            % Armazenamento das entradas do usuário
            self.nx = nx;  
            self.nu = nu;
            self.N = N;
            self.t0L = t0L;
            self.t0U = t0U;
            self.tfL = tfL;
            self.tfU = tfU;
            self.uL = uL;
            self.uU = uU;
            self.xL = xL;
            self.xU = xU;
            self.x0L = x0L;
            self.x0U = x0U;
            self.xfL = xfL;
            self.xfU = xfU;
            self.U0 = U0;
            self.X0 = X0;
            self.T0 = T0; 
            self.xNames = xNames;
            self.uNames = uNames;
        end 
    end
end

