classdef Util
    
    methods (Static) % -------------------------------------------- methods
        
        function v = vec(A) % ----------------------------------------- vec
            v = reshape(A, numel(A), 1);
        end
        
        function A = mat(v, n, m) % ----------------------------------- mat
            A = reshape(v, n, m);
        end
        
        function P = spline1(x, y) % ------------------------------ spline1
            % (x,y) - dados para interpolação
            % P - Matriz contendo em cada linha os coeficientes dos polinômios
            % interpoladores
            
            % Definição de variáveis auxiliares
            n = length(x) - 1; % Número de polinômios interpoladores
            nVar = 2*n; % Número de argumentos
            
            % Construção da matriz A
            A = zeros(nVar);
            j = 1;
            for i = 1:2:nVar
                A(i, i) = x(j);
                A(i, i+1) = 1;
                A(i+1, i) = x(j+1);
                A(i+1, i+1) = 1;
                
                j = j+1;
            end
            
            % Construção da matriz B
            B = zeros(nVar,1);
            j = 2;
            for i = 2:2:nVar-2
                B(i) = y(j);
                B(i+1) = y(j);
                j = j + 1;
            end
            B(1) = y(1);
            B(end) = y(end);
            
            % Computação dos coeficientes dos polinômios interpoladores
            C = A\B;
            P = reshape(C, 2, numel(C)/2)';
        end
        
        function P = spline2(xt, yt) % ---------------------------- spline2
            % (x,y) - dados para interpolação
            % P - Matriz contendo em cada linha os coeficientes dos polinômios
            % interpoladores
            
            if mod(length(xt), 2) == 0 % Se o número de elementos em x é par
                error(strcat('O número total de elementos (nós principais e', ...
                    'nós intermediários) tem de ser ímpar!'));
            end
            
            % Discriminação entre os nós principais e os nós intermediários
            x = xt(1:2:end);
            xMid = xt(2:2:end-1);
            y = yt(1:2:end);
            yMid = yt(2:2:end-1);
            
            % Definição de variáveis auxiliares
            n = length(x) - 1; % Número de polinômios interpoladores
            nVar = 3*n; % Número de argumentos
            
            % Construção das primeiras 2n linhas da matriz A
            A = zeros(nVar);
            j = 1;
            k = 1;
            for i = 1:2*n
                A(i, j) = x(k)^2;
                A(i, j+1) = x(k);
                A(i, j+2) = 1;
                
                if mod(i,2) ~= 0 % se i é ímpar
                    k = k + 1;
                end
                
                if mod(i,2) == 0 % se i é par
                    j = j + 3;
                end
            end
            
            % Construção das últimas n linhas da matriz A
            k = 1;
            j = 1;
            for i = (2*n + 1):(3*n)
                A(i,j) = xMid(k)^2;
                A(i,j+1) = xMid(k);
                A(i,j+2) = 1;
                
                k = k + 1;
                j = j + 3;
            end
            
            % Definição da matriz B
            B = zeros(nVar, 1);
            j = 2;
            for i = 2:2:2*n - 1
                B(i) = y(j);
                B(i+1) = y(j);
                j = j + 1;
            end
            B(1) = y(1);
            B(2*n) = y(end);
            B(end-n+1:end) = yMid;
            
            % Computação dos coeficientes dos polinômios interpoladores
            C = A\B;
            P = reshape(C, 3, numel(C)/3)';
        end
        
        function P = spline3(x, y, dy) % -------------------------- spline3
            % (x,y) - dados para interpolação
            % P - Matriz contendo em cada linha os coeficientes dos polinômios
            % interpoladores
            
            % Definição de variáveis auxiliares
            n = length(x) - 1; % Número de polinômios interpoladores
            nVar = 4*n; % Número de argumentos
            
            % Construção da primeira metade da matriz A
            A = zeros(nVar);
            j = 1;
            k = 1;
            for i = 1:nVar/2
                A(i, j) = x(k)^3;
                A(i, j+1) = x(k)^2;
                A(i, j+2) = x(k);
                A(i, j+3) = 1;
                
                if mod(i,2) ~= 0 % se i é ímpar
                    k = k + 1;
                end
                
                if mod(i,2) == 0 % se i é par
                    j = j + 4;
                end
            end
            
            % Construção da segunda metade da matriz A
            j = 1;
            k = 1;
            for i = nVar/2 + 1:nVar
                A(i, j) = 3*x(k)^2;
                A(i, j+1) = 2*x(k);
                A(i, j+2) = 1;
                
                if mod(i,2) ~= 0 % se i é ímpar
                    k = k + 1;
                end
                
                if mod(i,2) == 0 % se i é par
                    j = j + 4;
                end
            end
            
            % Construção da primeira metade da matriz B
            B = zeros(nVar, 1);
            j = 2;
            for i = 2 : 2 : nVar/2 - 1
                B(i) = y(j);
                B(i+1) = y(j);
                j = j + 1;
            end
            B(1) = y(1);
            B(nVar/2) = y(end);
            
            % Construção da segunda metade da matriz B
            j = 2;
            for i = nVar/2 + 2 : 2 : nVar - 1
                B(i) = dy(j);
                B(i+1) = dy(j);
                j = j + 1;
            end
            B(nVar/2 + 1) = dy(1);
            B(nVar) = dy(end);
            
            % Computação dos coeficientes dos polinômios interpoladores
            C = A\B;
            P = reshape(C, 4, numel(C)/4)';
        end
        
        function Yq = splineInterp(x, P, Xq) % --------------- splineInterp
            % Inicialização de Yq
            Yq = zeros(size(Xq));
            
            % Processo de interpolação
            for i = 1:length(Xq)
                % Avaliação de um elemento de Xq
                xq = Xq(i);
                
                % Determinação de qual dos polinômios em P deve ser
                % utilizado para a interpolação
                if xq == x(end)
                    index = length(x) - 1;
                else
                    indexFind = find(x <= xq);
                    index = indexFind(end);
                end
                
                % Determinação e armazenamento de yq
                p = P(index,:);
                yq = polyval(p, xq);
                Yq(i) = yq;
            end
        end
        
    end
end

