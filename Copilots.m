%#ok<*PROP>
%#ok<*AGROW>
classdef Copilots < handle
    
    properties % ----------------------------------------------- properties
        data;
        solver;
        solution;
        options;
        collocation; 
    end
   
    methods % ----------------------------------------------------- methods
        
        function copilots = Copilots() % ------------------------- Copilots
            % Leitura e tratamento dos parâmetros do problema
            data = Data;
            data.readInput();
            
            % Leitura e tratamento das configurações do solver NLP
            options = Options;
            options.readInput();
            
            % Criação do objeto Collocation
            if strcmp(options.collocationType, 'trap')
                fprintf('Colocação trapezoidal selecionada.\n');
                collocation = Trap(data, options);
            elseif strcmp(options.collocationType, 'hersim')
                fprintf('Colocação de Hermite-Simpson selecionada.\n');
                collocation = HerSim(data, options);
            end
            
            % Criação do objeto Solver
            solver = Solver(data, options, collocation);
            
            % Inicialização do objeto Copilots
            copilots.data = data;
            copilots.solver = solver;
            copilots.options = options;
            copilots.collocation = collocation; 
        end
       
        function solve(self) % -------------------------------------- solve
            % Atributos
            options = self.options;
            plotResults = options.plotResults;
            plotTraj = options.plotTraj;
            solver = self.solver;
            collocation = self.collocation;
            data = self.data; 
            
            % Criação da pasta results
            if ~exist('results', 'dir')
                mkdir('results');
                mkdir('results/solution')
                mkdir('results/solution/data');
                mkdir('results/solution/data/mid');
                mkdir('results/solution/plot');
                mkdir('results/solution/plot/fig');
                mkdir('results/traj/data');
                mkdir('results/traj/plot');
                mkdir('results/traj/plot/fig');
            end
            
            % Configuração do arquivo log que armazenará dados de execução
            if exist('results/log', 'file')
                delete('results/log');
            end 
            diary results/log;
            
            % Computação da solução
            solver.solve();
            diary off; 
            
            % Obtenção e armazenamento da solução
            solution = solver.solution;
            solution.data = data;
            solution.options = options;
            save results/solution solution
            
            % Verificação do sucesso da execução
            exitFlag = solution.exitFlag;
            
            % O tratamento da solução só deve ser realizado caso a execução
            % tenha sido bem sucedida
            if exitFlag ~= -1
                % Armazenamento da solução
                collocation.saveSolution();
                
                % Representação gráfica dos resultados
                if plotResults
                    collocation.plotSolution();
                end
                
                % Apresentação da trajetória contínua
                if plotTraj
                    collocation.plotTraj();
                end
            end
        end 
       
    end
    
end

