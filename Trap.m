%#ok<*PROPLC>
%#ok<*PROP>
%#ok<*AGROW>
classdef Trap < handle
    
    properties % ----------------------------------------------- properties
        data
        solution
        options
    end
    
    methods % ----------------------------------------------------- methods
        
        function trap = Trap(data, options) % ------------------------ Trap
            % Atualização dos atributos
            trap.data = data;
            trap.options = options;
        end
        
        function [lb, ub] = getBounds(self) % ------------------- getBounds
            % Atributos
            data = self.data; 
            N = data.N; 
            x0L = data.x0L;
            x0U = data.x0U;
            xL = data.xL;
            xU = data.xU; 
            xfL = data.xfL;
            xfU = data.xfU; 
            uL = data.uL; 
            uU = data.uU; 
            tfL = data.tfL;
            tfU = data.tfU; 
            t0L = data.t0L;
            t0U = data.t0U;
            
            % Computação dos limitantes inferiores em u, x e tf
            ulb = uL*ones(1, N);
            xlb = [x0L, xL*ones(1, N-2), xfL];
            t0lb = t0L;
            tflb = tfL; 
            
            % Computação dos limitantes superiores em u, x e tf
            uub = uU*ones(1, N);
            xub = [x0U, xU*ones(1, N-2), xfU];
            t0ub = t0U;
            tfub = tfU; 
            
            % Construção do vetor lb
            lb = [Util.vec(ulb); 
                  Util.vec(xlb); 
                  t0lb;
                  tflb];
              
            % Construção do vetor ub  
            ub = [Util.vec(uub); 
                  Util.vec(xub); 
                  t0ub;
                  tfub];
        end
        
        function z0 = initNLP(self) % ----------------------------- initNLP
            % Atributos
            data = self.data;
            U0 = data.U0;
            X0 = data.X0;
            T0 = data.T0;
            
            % Criação de z0
            z0 = [Util.vec(U0); 
                  Util.vec(X0);
                  T0(1);
                  T0(2)];
        end
        
        function solution = getSolution(self, z) % ------------ getSolution
            % Atributos
            getXU = @self.getXU;
            buildTraj = @self.buildTraj;
            getF = @self.getF;
            
            % Obtenção dos resultados do NLP
            [x, u, t] = getXU(z);
            f = getF(x, u, t);
            
            % Computação da trajetória contínua 
            [xc, uc, tc, Px, Pu] = buildTraj(x, u, t);
            
            % Atualiação da solução
            solution.x = x;
            solution.u = u;
            solution.t = t;
            solution.f = f;
            
            % Armazenamento da trajetória contínua no objeto solution
            contTraj.tc = tc;
            contTraj.xc = xc;
            contTraj.uc = uc;
            contTraj.Pu = Pu;
            contTraj.Px = Px; 
            solution.contTraj = contTraj;
            
            % Atualização dos atributos
            self.solution = solution;
        end
        
        function [intL, phi] = computeCost(self, z) % --------- computeCost
            % Atributos
            getXU = @self.getXU;
            computeInt = @self.computeInt;
            data = self.data;
            N = data.N;
            
            % Obtenção dos controles, estados e tempos
            [X, U, T] = getXU(z);
            
            % Computação do custo integral
            L = zeros(1, N);
            for k = 1:N
                L(k) = costL(X(:,k), U(:,k), T(k));
            end
            
            % Cálculo da integral
            intL = computeInt(T, L);
            
            % Cálculo do custo final
            xf = X(:,end);
            tf = T(end);
            phi = costPhi(xf, tf);
        end
        
        function [c, ceq] = computeConst(self, z) % ---------- computeConst
            % Atributos
            computDyn = @self.computDyn;
            data = self.data;
            N = data.N;
            
            % Obtenção dos controles, estados e tempos
            [X, U, T] = getXU(self, z);
            
            % Computação das restrições de caminho
            cP = [];
            ceqP = [];
            for i = 1:N
                [cPi, ceqPi] = pathConstraint(X(:,i), U(:,i), T(i));
                if ~isempty(cPi)
                    cP = [cP; cPi];
                end
                if ~isempty(ceqPi)
                    ceqP = [ceqP; ceqPi];
                end
            end
            
            % Determinação dos estados final e inicial
            x0 = X(:,1);
            xf = X(:,end);
            
            % Computação das restrições de contorno
            [cB, ceqB] = boundConstraint(x0, xf, tf);
            
            % Computação das restrições dinâmicas
            D = computDyn(X, U, T);
            vecD = Util.vec(D);
            
            % Verificação das restrições dinâmicas
            if ~all(isfinite(vecD))
                error(['Valor não numerico (Inf ou NaN) nos estados ',...
                    'ou em suas derivadas! \n\n']);
            end
            
            % Definição das restrições do problema
            c = [cB;       % Restrições de contorno
                 cP];      % Restrições de caminho
            
            ceq = [ceqB    % Restrições de contorno
                   ceqP    % Restrições de caminho
                   vecD];  % Restrições dinâmicas
        end
        
        function F = getF(self, X, U, T) % --------------------------- getF
            % Atributos
            data = self.data;
            N = data.N;
            nx = data.nx;
            
            % Computação da dinâmica
            F = zeros(nx, N);
            for k = 1:N
                F(:,k) = dynamics(X(:,k), U(:,k), T(k));
            end
        end
        
        function D = computDyn(self, X, U, T) % ----------------- computDyn
            % Atributos
            getF = @self.getF;
            data = self.data;
            N = data.N;
            nx = data.nx;
            
            % Computação da dinâmica
            F = getF(X, U, T);
            
            % Cálculo dos desvios na restrição dinâmica
            % Colocação trapezoidal
            % x(k+1) - x(k) = h/2 [f(k+1) + f(k)]
            H = diff(T);
            D = zeros(nx,N-1);
            for k = 1:N-1
                D(:,k) = (X(:,k+1) - X(:,k)) - ...
                         (H(k)/2)*(F(:,k+1) + F(:,k));
            end
        end
        
        function [X, U, T] = getXU(self, z) % ----------------------- getXU
            % Atributos
            data = self.data;
            nx = data.nx;
            nu = data.nu;
            N = data.N;
            
            % Obtenção de X, U, t0 e tf
            zU = z(1 : nu*N);
            zX = z(nu*N + 1 : end - 2);
            t0 = z(end-1);
            tf = z(end);
            
            % Reconstrução de X, U
            X = Util.mat(zX, nx, N);
            U = Util.mat(zU, nu, N);
            
            % Construção do vetor T
            T = linspace(t0, tf, N);
        end
        
        function int = computeInt(~, x, y) % ------------------- computeInt
            % Declaração de variáveis auxiliares
            m = length(x) - 1;
            h = (x(end) - x(1))/m;
            
            % Computação da integral
            int = (h/2)*(y(1) + 2*sum(y(2:end-1)) + y(end));
        end
        
        function saveSolution(self) % ------------------------ saveSolution
            % Atributos
            solution = self.solution;
            
            % Determinação da solução
            xOpt = solution.x;
            uOpt = solution.u;
            tOpt = solution.t;
            
            % Armazenamento da solução
            save results/solution/data/x.dat xOpt -ascii
            save results/solution/data/u.dat uOpt -ascii
            save results/solution/data/t.dat tOpt -ascii
        end
        
        function plotSolution(self) % ------------------------ plotSolution
            % Atributos
            solution = self.solution;
            options = self.options;
            data = self.data;
            xNames = data.xNames;
            uNames = data.uNames;
            plotResults = options.plotResults;
            
            % Determinação da trajetória ótima
            x = solution.x;
            u = solution.u;
            t = solution.t;
            
            if plotResults
                % Apresentação dos estados
                figure;
                plot(t, x, '.-', 'LineWidth', 1.5, 'MarkerSize', 15);
                title('x');
                xlabel('t (s)');
                legend(xNames);
                axis tight;
                grid on;
                print('results/solution/plot/x', '-dpng');
                savefig('results/solution/plot/fig/x');
                
                % Apresentação dos controles
                figure;
                plot(t, u, '.-', 'LineWidth', 1.5, 'MarkerSize', 15);
                title('u');
                xlabel('t (s)');
                legend(uNames);
                axis tight;
                grid on;
                print('results/solution/plot/u', '-dpng');
                savefig('results/solution/plot/fig/u');
            end
        end
        
        % ------------------------------------------------------- buildTraj
        function [xc, uc, tc, Px, Pu] = buildTraj(self, x, u, t) 
            % Atributos
            getF = @self.getF;
            spline1 = @Util.spline1;
            spline3 = @Util.spline3; 
            splineInterp = @Util.splineInterp;
            data = self.data;
            nx = data.nx;
            nu = data.nu;
            N = data.N;
            
            % Computação da dinâmica para construção da trajetória 
            f = getF(x, u, t);
            
            % Definição dos polinômios aproximadores dos estados
            tc = linspace(t(1), t(end), 1000*(N-1));
            xc = zeros(nx, 1000*(N-1));
            for i = 1:nx
                Px = spline3(t, x(i,:), f(i,:));
                xc(i,:) = splineInterp(t, Px, tc);
            end
            
            % Definição dos polinômios aproximadores dos constroles
            uc = zeros(nu, 1000*(N-1));
            for i = 1:nu
                Pu = spline1(t, u(i,:));
                uc(i,:) = splineInterp(t, Pu, tc);
            end
            
            % Armazenamento dos resultados em arquivos
            save results/traj/data/xc.dat xc -ascii
            save results/traj/data/uc.dat uc -ascii
            save results/traj/data/tc.dat tc -ascii
        end
        
        function plotTraj(self) % ------------------------------- buildTraj
            % Atributos
            solution = self.solution;
            data = self.data;
            options = self.options;
            nx = data.nx;
            nu = data.nu;
            xNames = data.xNames;
            uNames = data.uNames;
            plotTraj = options.plotTraj;
            
            % Obtenção dos dados da solução
            tc = solution.contTraj.tc;
            xc = solution.contTraj.xc;
            uc = solution.contTraj.uc;
            x = solution.x;
            u = solution.u;
            t = solution.t; 
            
            % Definição das cores utilizadas nos plots
            colors = [
                0     0.447 0.741
                0.85  0.325 0.098
                0.929 0.694 0.125
                0.494 0.184 0.556
                0.466 0.674 0.188
                0.301 0.745 0.933
                0.635 0.078 0.184];
            
            if plotTraj
                % Apresentação dos controles
                figure; hold on;
                idxColor = 1;
                plots = zeros(1,nx);
                for i = 1:nx
                    plots(i) = plot(tc, xc(i,:), 'Color', ...
                        colors(idxColor,:), 'LineWidth', 1.5);
                    plot(t, x(i,:), '.', 'Color', colors(idxColor,:), ...
                        'MarkerSize', 15);
                    
                    idxColor = idxColor + 1;
                    if idxColor == 8
                        idxColor = 1;
                    end
                end
                title('x');
                xlabel('t (s)');
                legend(plots, xNames);
                axis tight;
                grid on;
                print('results/traj/plot/xtraj', '-dpng');
                savefig('results/traj/plot/fig/xtraj')
                
                % Apresentação dos estados
                figure; hold on;
                idxColor = 1;
                plots = zeros(1,nu);
                for i = 1:nu
                    plots(i) = plot(tc, uc(i,:), 'Color', ...
                        colors(idxColor,:), 'LineWidth', 1.5);
                    plot(t, u(i,:), '.', 'Color', colors(idxColor,:), ...
                        'MarkerSize', 15);
                    
                    idxColor = idxColor + 1;
                    if idxColor == 8
                        idxColor = 1;
                    end
                end
                title('u');
                xlabel('t (s)');
                legend(plots, uNames);
                axis tight;
                grid on;
                print('results/traj/plot/utraj', '-dpng');
                savefig('results/traj/plot/fig/utraj')
            end
        end
        
    end
    
end

