%#ok<*PROPLC>
%#ok<*PROP>
%#ok<*AGROW>
classdef HerSim < handle
    
    properties % ----------------------------------------------- properties
        data
        solution
        options
    end
    
    methods % ----------------------------------------------------- methods
        
        function herSim = HerSim(data, options) % ------------------ HerSim
            % Atributos
            N = data.N;
            
            % Computação de Nt (número de elementos considerando tanto os
            % nós principais como dos intermediários)
            Nt = 2*N - 1;
            
            % Atualização dos atributos
            data.Nt = Nt;
            herSim.data = data;
            herSim.options = options;
        end
        
        function [lb, ub] = getBounds(self) % ------------------- getBounds
            % Atributos
            data = self.data;
            Nt = data.Nt;
            x0L = data.x0L;
            x0U = data.x0U;
            xL = data.xL;
            xU = data.xU;
            xfL = data.xfL;
            xfU = data.xfU;
            uL = data.uL;
            uU = data.uU;
            tfL = data.tfL;
            tfU = data.tfU;
            t0L = data.t0L;
            t0U = data.t0U;
            
            % Computação dos limitantes inferiores em u, x e tf
            ulb = uL*ones(1, Nt);
            xlb = [x0L, xL*ones(1, Nt-2), xfL];
            t0lb = t0L;
            tflb = tfL;
            
            % Computação dos limitantes superiores em u, x e tf
            uub = uU*ones(1, Nt);
            xub = [x0U, xU*ones(1, Nt-2), xfU];
            t0ub = t0U;
            tfub = tfU;
            
            % Construção do vetor lb
            lb = [Util.vec(ulb);
                Util.vec(xlb);
                t0lb;
                tflb];
            
            % Construção do vetor ub
            ub = [Util.vec(uub);
                Util.vec(xub);
                t0ub;
                tfub];
        end
        
        function [U0t, X0t] = initUX(self) % ---------------------- initU02
            % Atributos
            data = self.data;
            U0 = data.U0;
            X0 = data.X0;
            nu = data.nu;
            nx = data.nx;
            N = data.N;
            
            % Inicialização dos índices referentes aos elementos de U e U2
            idx = 1:N;
            idx2 = 1.5:N-0.5;
            
            % Construção de U20
            for i = 1:nu
                U02(i, :) = interp1(idx, U0(i, :), idx2);
            end
            
            % Construção de X20
            for i = 1:nx
                X02(i, :) = interp1(idx, X0(i, :), idx2);
            end
            
            % Inicialização de Ut0 e Xt0
            U0t = zeros(nu, 2*N - 1);
            X0t = zeros(nx, 2*N - 1);
            
            % Construção de Ut0 e Xt0
            U0t(:, 1:2:end) = U0;
            U0t(:, 2:2:end-1) = U02;
            X0t(:, 1:2:end) = X0;
            X0t(:, 2:2:end) = X02;
        end
        
        function z0 = initNLP(self) % ----------------------------- initNLP
            % Atributos
            initUX = @self.initUX;
            data = self.data;
            T0 = data.T0;
            
            % Criação dos novos U0 e X0 (diferentes do infomado pelo
            % usuário)
            [U0t, X0t] = initUX();
            
            % Criação de z0
            z0 = [Util.vec(U0t);
                Util.vec(X0t);
                T0(1);
                T0(2)];
        end
        
        function solution = getSolution(self, z) % ------------ getSolution
            % Atributos
            getF = @self.getF;
            getXU = @self.getXU;
            buildTraj = @self.buildTraj;
            
            % Obtenção dos resultados do NLP
            [xt, ut, tt] = getXU(z);
            [f, f2] = getF(xt, ut, tt);
            
            % Distinção entre nós principais e intermediários
            x = xt(:, 1:2:end);
            x2 = xt(:, 2:2:end-1);
            u = ut(:, 1:2:end);
            u2 = ut(:, 2:2:end-1);
            t = tt(1:2:end);
            t2 = tt(2:2:end-1);
            
            % Computação da trajetória contínua 
            [xc, uc, tc, Px, Pu] = buildTraj(xt, ut, tt);
            
            % Atualiação da solução
            solution.x = x;
            solution.u = u;
            solution.t = t;
            solution.x2 = x2;
            solution.u2 = u2;
            solution.t2 = t2;
            solution.f = f;
            solution.f2 = f2;
            
            % Armazenamento da trajetória contínua no objeto solution
            contTraj.tc = tc;
            contTraj.xc = xc;
            contTraj.uc = uc;
            contTraj.Pu = Pu;
            contTraj.Px = Px; 
            solution.contTraj = contTraj;
            
            % Atualização dos atributos
            self.solution = solution;
        end
        
        function [intL, phi] = computeCost(self, z) % --------- computeCost
            % Atributos
            getXU = @self.getXU;
            computeInt = @self.computeInt;
            data = self.data;
            Nt = data.Nt;
            
            % Obtenção dos controles, estados e tempos
            [Xt, Ut, Tt] = getXU(z);
            
            % Computação do custo integral L
            L = zeros(1, Nt);
            for k = 1:Nt
                L(k) = costL(Xt(:,k), Ut(:,k), Tt(k));
            end
            
            % Cálculo da integral
            intL = computeInt(Tt, L);
            
            % Cálculo do custo final
            tf = Tt(end);
            xf = Xt(:,end);
            phi = costPhi(xf, tf);
        end
        
        function [c, ceq] = computeConst(self, z) % ---------- computeConst
            % Atributos
            computInterp = @self.computInterp;
            computDyn = @self.computDyn;
            data = self.data;
            Nt = data.Nt;
            
            % Obtenção dos controles, estados e tempos
            [Xt, Ut, Tt] = getXU(self, z);
            
            % Determinação dos estados final e inicial
            x0 = Xt(:,1);
            xf = Xt(:,end);
            tf = Tt(end);
            
            % Computação das restrições de contorno
            [cB, ceqB] = boundConstraint(x0, xf, tf);
            
            % Computação das restrições de caminho para X e U
            cP = [];
            ceqP = [];
            for i = 1:Nt
                [cPi, ceqPi] = pathConstraint(Xt(:,i), Ut(:,i), Tt(i));
                if ~isempty(cPi)
                    cP = [cP; cPi];
                end
                if ~isempty(ceqPi)
                    ceqP = [ceqP; ceqPi];
                end
            end
            
            % Computação das restrições dinâmicas
            D = computDyn(Xt, Ut, Tt);
            vecD = Util.vec(D);
            
            % Computação das restrições de interpolação
            Di = computInterp(Xt, Ut, Tt);
            vecDi = Util.vec(Di);
            
            % Verificação das restrições dinâmicas
            if ~all(isfinite(vecD))
                error(['Valor nao numerico (Inf ou NaN) nos estados ',...
                    'ou em suas derivadas! \n\n']);
            end
            
            % Definição das restrições do problema
            c = [cB;            % Restrições de contorno
                cP];           % Restrições de caminho
            
            ceq = [ceqB;        % Restrições de contorno
                ceqP;        % Restrições de caminho
                vecD;        % Restrições dinâmicas
                vecDi];      % Restrições de interpolação
        end
        
        function Di = computInterp(self, Xt, Ut, Tt) % ------- computInterp
            % Atributos
            data = self.data;
            N = data.N;
            Nt = data.Nt;
            nx = data.nx;
            
            % Computação da dinâmica do sistema para Xt, Ut e Tt
            Ft = zeros(nx, Nt);
            for k = 1:Nt
                Ft(:,k) = dynamics(Xt(:,k), Ut(:,k), Tt(k));
            end
            
            % Distinção entre nós principais e intermediários
            T = Tt(1:2:end);
            X = Xt(:, 1:2:end);
            X2 = Xt(:, 2:2:end-1);
            F = Ft(:, 1:2:end);
            
            % Interpolação de hermite
            % x(k+1) - x(k) = h/6 [f(k) + 4f(k+1/2) + f(k+1)]
            H = diff(T);
            Di = zeros(nx, N-1);
            for k = 1:N-1
                Di(:,k) = X2(:,k) - (1/2)*(X(:,k) + X(:,k+1)) - ...
                    (H(k)/8)*(F(:,k) - F(:,k+1));
            end
        end
        
        function D = computDyn(self, Xt, Ut, Tt) % -------------- computDyn
            % Atributos
            getF = @self.getF; 
            data = self.data;
            N = data.N;
            nx = data.nx;
            
            % Computação da dinâmica do sistema para Xt, Ut e Tt
            [F, F2] = getF(Xt, Ut, Tt);
            
            % Distinção entre nós principais e intermediários
            X = Xt(:, 1:2:end);
            T = Tt(1:2:end);
            
            % Colocação de simpson
            % x(k+1) - x(k) = h/6 [f(k) + 4f(k+1/2) + f(k+1)]
            H = diff(T);
            D = zeros(nx, N-1);
            for k = 1:N-1
                D(:,k) = (X(:,k+1) - X(:,k)) - ...
                    (H(k)/6)*(F(:,k) + 4*F2(:,k) + F(:,k+1));
            end
        end
        
        function [F, F2, Ft] = getF(self, Xt, Ut, Tt) % -------------- getF
            % Atributos
            data = self.data;
            Nt = data.Nt;
            nx = data.nx;
            
            % Computação da dinâmica do sistema para Xt, Ut e Tt
            Ft = zeros(nx, Nt);
            for k = 1:Nt
                Ft(:,k) = dynamics(Xt(:,k), Ut(:,k), Tt(k));
            end
            
            % Distinção entre nós principais e intermediários
            F = Ft(:, 1:2:end);
            F2 = Ft(:, 2:2:end-1);
        end
        
        function [Xt, Ut, Tt] = getXU(self, z) % -------------------- getXU
            % Atributos
            data = self.data;
            nx = data.nx;
            nu = data.nu;
            Nt = data.Nt;
            
            % Cálculo dos índices correspondentes a U, X e U2 no vetor z
            iU0 = 1;
            iUf = nu*Nt;
            iX0 = iUf + 1;
            iXf = iX0 + (nx*Nt) - 1;
            
            % Desconstrução do vetor z
            zU = z(iU0 : iUf);
            zX = z(iX0 : iXf);
            t0 = z(end-1);
            tf = z(end);
            
            % Determinação de X, U, U2 e T
            Tt = linspace(t0, tf, Nt);
            Xt = Util.mat(zX, nx, Nt);
            Ut = Util.mat(zU, nu, Nt);
        end
        
        function int = computeInt(~, x, y) % ------------------- computeInt
            % (x0, y0), (x1, y1), ..., (xm, ym)
            % Número de elementos tem de ser ímpar (m tem de ser par)
            if mod(length(y),2) == 0
                error('O número de elementos tem de ser ímpar para integração!');
            end
            
            % Cálculo de variáveis auxiliares
            m = length(x) - 1;
            h = (x(end) - x(1))/m;
            
            yEven = y(2:2:end-1);
            yOdd = y(3:2:end-2);
            
            % Computação da integral pela quadratura de simpson
            int = (h/3)*(y(1) + y(end) + 4*sum(yEven) + 2*sum(yOdd));
            
        end
        
        function saveSolution(self) % ------------------------ saveSolution
            % Atributos
            solution = self.solution;
            
            % Determinação da solução
            x = solution.x;
            u = solution.u;
            t = solution.t;
            x2 = solution.x2;
            u2 = solution.u2;
            t2 = solution.t2;
            
            % Armazenamento da solução
            save results/solution/data/x.dat x -ascii
            save results/solution/data/u.dat u -ascii
            save results/solution/data/t.dat t -ascii
            
            % Armazenamento da solução (nós intermediários)
            save results/solution/data/mid/x2.dat x2 -ascii
            save results/solution/data/mid/u2.dat u2 -ascii
            save results/solution/data/mid/t2.dat t2 -ascii
        end
        
        function plotSolution(self) % ------------------------ plotSolution
            % Atributos
            solution = self.solution;
            options = self.options;
            data = self.data;
            xNames = data.xNames;
            uNames = data.uNames;
            plotResults = options.plotResults;
            
            % Determinação da solução
            x = solution.x;
            u = solution.u;
            t = solution.t;
            
            if plotResults
                % Apresentação dos estados
                figure;
                plot(t, x, '.-', 'LineWidth', 1.5, 'MarkerSize', 15);
                title('x');
                xlabel('t (s)');
                legend(xNames);
                axis tight;
                grid on;
                print('results/solution/plot/x', '-dpng');
                savefig('results/solution/plot/fig/x');
                
                % Apresentação dos controles
                figure;
                plot(t, u, '.-', 'LineWidth', 1.5, 'MarkerSize', 15);
                title('u');
                xlabel('t (s)');
                legend(uNames);
                axis tight;
                grid on;
                print('results/solution/plot/u', '-dpng');
                savefig('results/solution/plot/fig/u');
            end
        end
        
        % ------------------------------------------------------- buildTraj
        function [xc, uc, tc, Px, Pu] = buildTraj(self, xt, ut, tt) 
            % Atributos
            getF = @self.getF;
            spline2 = @Util.spline2;
            spline3 = @Util.spline3; 
            splineInterp = @Util.splineInterp;
            data = self.data;
            nx = data.nx;
            nu = data.nu;
            N = data.N;
            
            % Determinação dos estados e tempos nos nós principais
            x = xt(:, 1:2:end);
            t = tt(1:2:end);
            
            % Computação da dinâmica para construção da trajetória 
            f = getF(xt, ut, tt);
            
            % Definição dos polinômios aproximadores dos estados
            tc = linspace(t(1), t(end), 1000*(N-1));
            xc = zeros(nx, 1000*(N-1));
            for i = 1:nx
                Px = spline3(t, x(i,:), f(i,:));
                xc(i,:) = splineInterp(t, Px, tc);
            end
            
            % Definição dos polinômios aproximadores dos constroles
            uc = zeros(nu, 1000*(N-1));
            for i = 1:nu
                Pu = spline2(tt, ut(i,:));
                uc(i,:) = splineInterp(t, Pu, tc);
            end
            
            % Armazenamento dos resultados em arquivos
            save results/traj/data/xc.dat xc -ascii
            save results/traj/data/uc.dat uc -ascii
            save results/traj/data/tc.dat tc -ascii
        end
        
        function plotTraj(self)% --------------------------------- plotTraj
            % Atributos
            solution = self.solution;
            data = self.data;
            options = self.options;
            nx = data.nx;
            nu = data.nu;
            xNames = data.xNames;
            uNames = data.uNames;
            plotTraj = options.plotTraj;
            plotTrajMidpoints = options.plotTrajMidpoints;
            
            % Obtenção dos dados da solução
            tc = solution.contTraj.tc;
            xc = solution.contTraj.xc;
            uc = solution.contTraj.uc;
            x = solution.x;
            u = solution.u;
            t = solution.t; 
            u2 = solution.u2;
            t2 = solution.t2;
            
            % Definição das cores utilizadas nos plots
            colors = [
                0     0.447 0.741
                0.85  0.325 0.098
                0.929 0.694 0.125
                0.494 0.184 0.556
                0.466 0.674 0.188
                0.301 0.745 0.933
                0.635 0.078 0.184];
            
            if plotTraj
                % Apresentação dos estados
                figure; hold on;
                idxColor = 1;
                plots = zeros(1,nx);
                for i = 1:nx
                    plots(i) = plot(tc, xc(i,:), 'Color', ...
                        colors(idxColor,:), 'LineWidth', 1.5);
                    plot(t, x(i,:), '.', 'Color', colors(idxColor,:), ...
                        'MarkerSize', 15);
                    
                    idxColor = idxColor + 1;
                    if idxColor == 8
                        idxColor = 1;
                    end
                end
                title('x');
                xlabel('t (s)');
                legend(plots, xNames);
                axis tight;
                grid on;
                print('results/traj/plot/xtraj', '-dpng');
                savefig('results/traj/plot/fig/xtraj')
                
                % Apresentação dos controles
                figure; hold on;
                idxColor = 1;
                plots = zeros(1,nu);
                for i = 1:nu
                    plots(i) = plot(tc, uc(i,:), 'Color', ...
                        colors(idxColor,:), 'LineWidth', 1.5);
                    plot(t, u(i,:), '.', 'Color', colors(idxColor,:), ...
                        'MarkerSize', 15);
                    
                    if plotTrajMidpoints
                        plot(t2, u2(i,:), 's', 'Color', ...
                            colors(idxColor,:), 'LineWidth', 1.5);
                    end
                    
                    idxColor = idxColor + 1;
                    if idxColor == 8
                        idxColor = 1;
                    end
                end
                title('u');
                xlabel('t (s)');
                legend(plots, uNames);
                axis tight;
                grid on;
                print('results/traj/plot/utraj', '-dpng');
                savefig('results/traj/plot/fig/utraj')
            end
        end
        
    end
    
end

