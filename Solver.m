%#ok<*PROP>
%#ok<*PROPLC>
%#ok<*AGROW>
classdef Solver < handle

    properties % ----------------------------------------------- properties
        data
        solution
        quadrature
        collocation
        options
    end

    methods % ----------------------------------------------------- methods
        
        function solver = Solver(data, options, collocation) % ----- Solver
            % Atualização dos atributos
            solver.data = data;
            solver.options = options;
            solver.collocation = collocation;
        end 
        
        function solve(self) % -------------------------------------- solve
            % Atributos
            J = @self.objFun;
            c = @self.const;
            collocation = self.collocation;
            options = self.options;
            maxTime = options.maxTime; 
            
            % Inicialização do NLP
            z0 = collocation.initNLP();
            opt = options.getFminOptions();
            [lb, ub] = collocation.getBounds(); 
            
            % Construção do problema a ser passado ao NLP
            problem.objective = J;
            problem.x0 = z0;
            problem.Aineq = [];
            problem.bineq = [];
            problem.Aeq = [];
            problem.beq = [];
            problem.lb = lb;
            problem.ub = ub;
            problem.nonlcon = c;
            problem.solver = 'fmincon';
            problem.options = opt;
            
            tic;
            try
                % Solução do NLP
                [z, Jopt, exitFlag, out] = fmincon(problem);
            catch exeption
                % Verificação do tipo de erro obtido
                if strcmp(exeption.message, 'maxTime!')
                    fprintf('O tempo máximo ');
                    fprintf('de %f segundos foi ultrapasado\n\n', maxTime);
                    exitFlag = -1;
                else
                    fprintf(exeption.message);
                    exitFlag = -3;
                end
                
                % Atribuição de vetores vazios a todas as variáveis que
                % deveriam advir da solução da chamada de fmincon
                z = z0;
                Jopt = [];
                out.iterations = [];
                out.funcCount = [];
                out.constrviolation = [];
            end
            time = toc;
            
            % Construção da mensagem de saída
            switch exitFlag
                case 1
                    exitMsg = 'Solução obtida com sucesso!';
                case 0 
                    exitMsg = ['Número máximo de iterações ou de ', ...
                        'avaliações da função objetivo ultrapassado!'];
                case -1
                    exitMsg = 'Tempo máximo ultrapassado!';
                case -2 
                    exitMsg = ['Não foi possível encontrar uma solução', ...
                        ' que satisfizesse todas as restrições!'];
                otherwise
                    exitMsg = '';
            end

            % Apresentação da saída do processo de otimização
            iter = out.iterations;
            neval = out.funcCount;
            constDef = out.constrviolation;
            
            fprintf('Valor da função objetivo: %.10f \n', Jopt);
            fprintf('Número de iterações: %g \n', iter);
            fprintf('Número de avaliações: %g \n', neval);
            fprintf('Violação das restrições: %g \n', constDef);
            fprintf('Tempo de execução: %g \n', time);
            fprintf(strcat(exitMsg, '\n'));
            
            % Contrução da solução 
            solution = collocation.getSolution(z);
            solution.Jopt = Jopt;
            solution.iter = iter;
            solution.nval = neval;
            solution.constDef = constDef; 
            solution.time = time;
            solution.exitFlag = exitFlag;
            solution.exitMsg = exitMsg;
            solution.out = out;
            
            % Atualização dos atributos
            self.solution = solution;
        end 
  
        function J = objFun(self, z) % ----------------------------- objFun
            % Atributos
            options = self.options;
            maxTime = options.maxTime;
            collocation = self.collocation;
            
            % Verificação do tempo máximo
            passTime = toc; 
            
            if passTime > maxTime
                error('maxTime!');
            end
            
            % Computação dos custos integral e final
            [intL, phi] = collocation.computeCost(z);
            
            % Computação do custo total
            J = phi + intL;
        end 
       
        function [c, ceq] = const(self, z) % ------------------------ const
            % Atributos
            collocation = self.collocation;
            
            % Computação das restrições
            [c, ceq] = collocation.computeConst(z);
        end 
        
    end 
end 

