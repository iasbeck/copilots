nx = []; % Número de estados
nu = []; % Número de controles
N = []; % Número de nós utilizado na colocação

t0L = []; % Limite inferior para o tempo inicial
t0U = []; % Limite superior para o tempo inicial

tfL = []; % Limite inferior para o tempo final
tfU = []; % Limite superior para o tempo final

uL = []; % Limites inferiores para os controles
uU = []; % Limites superiores para os controles

xL = []; % Limites inferiores para os estados 
xU = []; % Limites superiores para os estados

x0L = []; % Limites inferiores para os estados iniciais
x0U = []; % Limites superiores para os estados iniciais

xfL = []; % Limites inferiores para os estados finais
xfU = []; % Limites superiores para os estados finais

U0 = []; % Palpites iniciais para os controles
X0 = []; % Palpites iniciais para os estados
T0 = []; % Palpite inicial para os tempos inicial e final

xNames = {}; % Nomes atribuídos ao estados
uNames = {}; % Nomes atribuídos aos controles