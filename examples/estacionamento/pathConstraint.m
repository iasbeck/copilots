% Esta função possibilita a inserção de restrições de caminho. Podemos
% definir restrições de igualdade (h = 0) ou de desigualdade (g <= 0).
% Considere que tenhamos várias restrições de igualdade e desigualdade
%
% h1 = 0
% h2 = 0
% h3 = 0
%
% g1 <= 0
% g2 <= 0
% g3 <= 0
%
% Neste caso devemos definir c e ceq como
%
% c = [g1; g2; g3]
% ceq = [h1; h2; h3].

function [c, ceq] = pathConstraint(x, u, t)

% Importação de constantes globais
global cte
l = cte.l;
m = cte.m;
n = cte.n;
b = cte.b;
CL = cte.CL;
SL = cte.SL;
dk_max = 0.6;
FULL_PROBLEM = cte.FULL_PROBLEM;

% Obtenção dos controles e estados
x1 = x(1);
x2 = x(2);
x5 = x(5);
x6 = x(6);

u2 = u(2);

% Definição de variáveis auxiliares
Ax = x1 + cos(x5)*(l + n) - b*sin(x5);
Ay = x2 + sin(x5)*(l + n) + b*cos(x5);
Bx = x1 + cos(x5)*(l + n) + b*sin(x5);
By = x2 + sin(x5)*(l + n) - b*cos(x5);
Cx = x1 - m*cos(x5) + b*sin(x5);
Cy = x2 - m*sin(x5) - b*cos(x5);
Dx = x1 - m*cos(x5) - b*sin(x5);
Dy = x2 - m*sin(x5) + b*cos(x5);

Ox = -x1*cos(x5) - x2*sin(x5) - ((l + n - m)/2);
Oy = x1*sin(x5) - x2*cos(x5);
Ex = -x1*cos(x5) - x2*sin(x5) - ((l + n - m)/2) + SL*cos(x5);
Ey = x1*sin(x5) - x2*cos(x5) - SL*sin(x5);

% Definição das restrições
% |k| <= dk_max === -dk_max <= k <= dk_max ===
%               === -dk_max - k <= 0 e k - dk_max <= 0
% Ay <= CL
% By <= CL
% Cy <= CL
% Dy <= CL
% slot(Ax) <= Ay === slot(Ax) - Ay <= 0
% slot(Bx) <= By === slot(Bx) - By <= 0
% slot(Cx) <= Cy === slot(Cx) - Cy <= 0
% slot(Dx) <= Dy === slot(Dx) - Dy <= 0
% |Ox'| >= (l+m+n)/2 --- se |Oy'| <= b
% |Ex'| >= (l+m+n)/2 --- se |Ey'| <= b

k = u2/(l*cos(x6)*cos(x6));

ceq = [];
c = [-dk_max - k
    k - dk_max
    Ay - CL
    By - CL
    Cy - CL
    Dy - CL
    slot(Ax) - Ay
    slot(Bx) - By
    slot(Cx) - Cy
    slot(Dx) - Dy];

if FULL_PROBLEM
    pOx = htg(Ox)*Ox*bandPass(Oy);
    pEx = htg(Ex)*Ex*bandPass(Ey);
    c(end+1) = (l+m+n)/2 - pOx;
    c(end+1) = (l+m+n)/2 - pEx; 
end

end