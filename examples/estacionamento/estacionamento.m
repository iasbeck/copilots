clc; clear; close all; 

% Importação das funções auxiliares
addpath('auxFunc')

% Declaração de constantes globais referentes ao modelo
global cte
cte.l = 2.588;
cte.m = 0.657;
cte.n = 0.839;
cte.b = 0.8855;
cte.da_max = 0.5;
cte.v_max = 2.0;
cte.a_max = 0.75;
cte.phi_max = 0.575959;
cte.SW = 2;
cte.CL = 3.5;
cte.SL = 6;

% Declaração de constantes globais referentes ao controle de execução
cte.FULL_PROBLEM = 1;
cte.INITIAL_GUESS = 1;

% Solução do problema
copilots;

% Armazenamento da solução para posterior utilização como palpite inicial
if ~cte.INITIAL_GUESS
    load('results/solution.mat');
    X0 = solution.x;
    U0 = solution.u;
    t0 = solution.t(1);
    tF = solution.t(end);
    T0 = [t0; tF];
    save initialGuess X0 U0 T0
end