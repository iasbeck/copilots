init; 
u = load('u.dat');
x = load('x.dat');
t = load('t.dat');
u2 = load('mid/u2.dat');
x2 = load('mid/x2.dat');
t2 = load('mid/t2.dat');

Nt = length(x) + length(x2);
nx = size(x, 1);
nu = size(u, 1);
X = zeros(nx, Nt);
U = zeros(nu, Nt);
T = zeros(1, Nt);

X(:, 1:2:end) = x;
X(:, 2:2:end-1) = x2;
U(:, 1:2:end) = u;
U(:, 2:2:end-1) = u2;
T(:, 1:2:end) = t;
T(:, 2:2:end-1) = t2;

Nspl = 1000;
Tspl = linspace(T(1), T(end), Nspl);

Xspl = zeros(nx, Nspl);
for i = 1:nx
    Xspl(i,:) = spline(T, X(i,:), Tspl);
end

Uspl = zeros(nu, Nspl);
for i = 1:nu
    Uspl(i,:) = spline2(T, U(i,:), Tspl);
end

figure; hold on;
plot(Tspl, Xspl, 'LineWidth', 1.5);
plot(T, X, '.', 'MarkerSize', 15)

figure; hold on;
plot(Tspl, Uspl(2,:), 'LineWidth', 1.5);
plot(T, U(2,:), '.', 'MarkerSize', 15)