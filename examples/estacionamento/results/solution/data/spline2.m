function Yq = spline2(x,y,Xq)

% Interpolação quadrática: Considerando que tenhamos os pontos (x0, y0),
% (x1, y1), (x2, y2) e (x3, y3), teremos que determinar três polinômios
% para ligação entre os pontos. Consideramos neste caso que a primeira
% spline é uma reta. Neste caso, teremos o seguinte sistema
%
% A = [x0^2 x0 1 0 0 0 0 0 0;
%      x1^2 x1 1 0 0 0 0 0 0;
%      0 0 0 x1^2 x1 1 0 0 0;
%      0 0 0 x2^2 x2 1 0 0 0;
%      0 0 0 0 0 0 x2^2 x2 1;
%      0 0 0 0 0 0 x3^2 x3 1;
%      2*x1 1 0 -2*x1 -1 0 0 0 0;
%      0 0 0 2*x2 1 0 -2*x2 -1 0;
%      1 0 0 0 0 0 0 0 0];
%
% B = [y0;
%      y1;
%      y1;
%      y2;
%      y2;
%      y3;
%      0;
%      0;
%      0];

% Maior índice: se temos 4 pontos, estes serão (x0, x1, x2, x3). É
% equivalente ao número de segumentos e de equações
n = length(x) - 1; 

% Equações de conexão entre os pontos (primeiras 2n linhas da matriz A)
A = zeros(3*n,3*n);
i = 1; % Linha
j = 1; % Coluna
k = 1; % Posição no vetor x
while 1
    % Construção da matriz A (de duas em duas linhas)
    A(i, j) = x(k)^2;
    A(i, j+1) = x(k);
    A(i, j+2) = 1;
    
    A(i+1, j) = x(k+1)^2;
    A(i+1, j+1) = x(k+1);
    A(i+1, j+2) = 1;
    
    % Verificação da condição de parada
    if i + 1 == 2*n
        i = i + 2;
        break
    end
    
    % Atualização das variáveis utilizadas no controle da execução
    k = k + 1;
    i = i + 2;
    j = j + 3;
   
end

% Equações de conexão da derivada entre os pontos (próximas n - 1 linhas da
% matriz A)
j = 1; % Coluna
k = 2; % Posição no vetor x
while 1
    % Construção da matriz A (de uma em uma linha)
    A(i, j) = 2*x(k);
    A(i, j+1) = 1;
    
    A(i, j+3) = -2*x(k);
    A(i, j+4) = -1;
    
    % Verificação do critério de parada
    if i == 3*n - 1
        i = i + 1;
        break
    end
    
    % Atualização das variáveis para controle de execução
    k = k + 1;
    i = i + 1;
    j = j + 3;

end

% Condição extra (primeira spline é uma reta)
A(i, 1) = 1;

% Construção do vetor coluna B
B = zeros(3*n,1);
i = 2; % Linha
k = 2; % Posição no vetor y
while 1
    % Construção da matriz B (de duas em duas linhas)
    B(i) = y(k);
    B(i+1) = y(k);
    
    % Verificação da condição de parada
    if i + 1 == 2*n - 1
        break
    end
    
    % Atualização das variáveis para controle de execução
    k = k + 1;
    i = i + 2;
end
% Finalização da matriz B
B(1) = y(1);
B(2*n) = y(end);

% Computação dos coeficientes de todas as splines e construção do vetor P,
% que guarda em cada uma de suas linhas os coeficientes de cada spline
C = A\B;
P = reshape(C, 3, numel(C)/3)';

% Computação de Yq a partir das splines determinadas e dos valores de Xq
Yq = zeros(size(Xq));
for i = 1:length(Xq)
    xq = Xq(i);
    if xq == x(end)
        index = length(x) - 1;
    else
        indexFind = find(x <= xq);
        index = indexFind(end);
    end
    p = P(index,:);
    yq = polyval(p, xq);
    Yq(i) = yq;
end

end