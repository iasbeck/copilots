% Declaração de constantes locais
global cte
da_max = cte.da_max;
v_max = cte.v_max;
a_max = cte.a_max;
phi_max = cte.phi_max;
SW = cte.SW;
CL = cte.CL;
SL = cte.SL;
m = cte.m;
n = cte.n;
l = cte.l;
b = cte.b;
INITIAL_GUESS = cte.INITIAL_GUESS;

% Definindo parâmetros de execução
nx = 6; % Número de estados
nu = 2; % Número de controles
N = 15; % Número de nós utilizado na colocação

t0L = 0; % Limite inferior para o tempo final
t0U = 0; % Limite superior para o tempo inicial

tfL = 1; % Limite inferior para o tempo final
tfU = 40; % Limite superior para o tempo inicial

uL = [-da_max; -inf]; % Limites inferiores para os controles
uU = [da_max; inf]; % Limites superiores para os controles

xL = [-15; -(SW - b); -v_max; -a_max; -inf; -phi_max]; % Limites inferiores para os estados 
xU = [15; CL - b; v_max; a_max; inf; phi_max]; % Limites superiores para os estados

x0L = [SL + m; 1.5; 0; 0; 0; 0]; % Limites inferiores para os estados iniciais
x0U = [SL + m; 1.5; 0; 0; 0; 0]; % Limites superiores para os estados iniciais

xfL = [m; -(SW - b); 0; 0; 0; -phi_max]; % Limites inferiores para os estados finais
xfU = [SL - (l + n); -b; 0; 0; 0; phi_max]; % Limites superiores para os estados finais

if INITIAL_GUESS
    load initialGuess
else
    U0 = zeros(2, N); % Palpites iniciais para os controles
    X0 = [linspace(x0L(1), xfL(1), N); % Palpites iniciais para os estados
          linspace(x0L(2), xfL(2), N);
          linspace(x0L(3), xfL(3), N);
          linspace(x0L(4), xfL(4), N);
          linspace(x0L(5), xfL(5), N);
          linspace(x0L(6), xfL(6), N)]; 
    T0 = [0; 10]; % Palpite inicial para os tempos inicial e final
end

xNames = {'x1', 'x2', 'x3', 'x4', 'x5', 'x6'}; % Nomes atribuídos ao estados
uNames = {'u1', 'u2'}; % Nomes atribuídos aos controles