% Aqui devem ser inseridas as equações diferenciais que descrevem o
% comportamento dinâmico do sistema
function dx = dynamics(x, u, t)

% x - Vetor (nx, 1)
% u - Vetor (nu,1)
% t - Variável temporal

global cte
l = cte.l;

% Obtenção dos estados e controles
x3 = x(3);
x4 = x(4);
x5 = x(5);
x6 = x(6);

u1 = u(1);
u2 = u(2);

% Definição das equações que ditam o comportamento dinâmico do sistema
dx1 = x3*cos(x5);
dx2 = x3*sin(x5);
dx3 = x4;
dx4 = u1;
dx5 = (x3*tan(x6))/l;
dx6 = u2;

dx = [dx1; dx2; dx3; dx4; dx5; dx6];

end