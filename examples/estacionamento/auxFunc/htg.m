function y = htg(x)

% global Atg
Atg = 10;
y = (exp(Atg*x) - 1)/(exp(Atg*x) + 1);

end