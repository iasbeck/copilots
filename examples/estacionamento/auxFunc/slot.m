function y = slot(x)

A = 10; % Inclinação da sigmoide (aproximação para o degrau)
SL = 6;
SW = 2;

y = (-sigmoid(x, A, 0) + sigmoid(x, A, SL))*SW;

end