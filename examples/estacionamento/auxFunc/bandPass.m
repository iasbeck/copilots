function y = bandPass(x)

global cte
b = cte.b;
AMP_SIG_CONST = 40;
constA = 10;

y = 1 + (AMP_SIG_CONST - 1)*(1 - sigmoid(x, constA, -b) + sigmoid(x, constA, b));

end