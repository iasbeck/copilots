% Importação de variáveis globais
global cte
d_max = cte.d_max;
u_max = cte.u_max;
d = cte.d;
T = cte.T; 

nx = 4; % Número de estados
nu = 1; % Número de controles
N = 15; % Número de nós utilizado na colocação

t0L = 0; % Limite inferior para o tempo final
t0U = 0; % Limite superior para o tempo inicial

tfL = T; % Limite inferior para o tempo final
tfU = T; % Limite superior para o tempo inicial

uL = -u_max; % Limites inferiores para os controles
uU = u_max; % Limites superiores para os controles

xL = [-d_max; -2*pi; -inf; -inf]; % Limites inferiores para os estados 
xU = [d_max; 2*pi; inf; inf]; % Limites superiores para os estados

x0L = [0; 0; 0; 0];  % Limites inferiores para os estados iniciais
x0U = [0; 0; 0; 0];  % Limites superiores para os estados iniciais

xfL = [d; pi; 0; 0]; % Limites inferiores para os estados finais
xfU = [d; pi; 0; 0]; % Limites superiores para os estados finais

U0 = zeros(1, N); % Palpites iniciais para os controles
X0 = [linspace(0, d, N);  % Palpites iniciais para os estados
      linspace(0, pi, N);
      linspace(0, 0, N);
      linspace(0, 0, N)];
T0 = [0; T]; % Palpite inicial para os tempos inicial e final

xNames = {'x1', 'x2', 'x3', 'x4'}; % Nomes atribuídos ao estados
uNames = {'u'};  % Nomes atribuídos aos controles