clc; clear; close all; 

% Declaração de constantes globais
global cte
cte.l = 0.5;
cte.m1 = 1;
cte.m2 = 0.3;
cte.g = 9.81;
cte.d_max = 2;
cte.u_max = 100;
cte.d = 1;
cte.T = 2; 

% Solução do problema de otimização
copilots;