% Aqui devem ser inseridas as equações diferenciais que descrevem o
% comportamento dinâmico do sistema
function dx = dynamics(x, u, t)

% x - Vetor (nx, 1)
% u - Vetor (nu,1)
% t - Variável temporal

% Importação de constantes globais
global cte
l = cte.l;
m1 = cte.m1;
m2 = cte.m2;
g = cte.g;

% Obtenção dos estados e controles
x2 = x(2);
x3 = x(3);
x4 = x(4);

u1 = u(1);

% Definição das equações que descrevem a dinâmica do sistema
dx1 = x3;
dx2 = x4;
dx3 = (l*m2*sin(x2)*x4^2 + u1 + g*m2*cos(x2)*sin(x2))/(m1 + m2 - m2*cos(x2)^2);
dx4 = -(l*m2*cos(x2)*sin(x2)*x4^2 + u1*cos(x2) + g*m1*sin(x2) + g*m2*sin(x2))/(l*(m1 + m2 - m2*cos(x2)^2));

dx = [dx1; dx2; dx3; dx4];

end