%#ok<*INUSD>
%#ok<*UNRCH>
%#ok<*NASGU>
%#ok<*INUSL>

% Esta função possibilita a inserção de restrições terminais. Podemos
% definir restrições de igualdade (h = 0) ou de desigualdade (g <= 0).
% Considere que tenhamos várias restrições de igualdade e desigualdade
%
% h1 = 0
% h2 = 0
% h3 = 0
%
% g1 <= 0
% g2 <= 0
% g3 <= 0
%
% Devemos definir c e ceq como
%
% c = [g1; g2; g3]
% ceq = [h1; h2; h3].

function [c, ceq] = boundConstraint(x0, xf, tf) 

% Importação de constantes globais
global cte
x2_0 = cte.x2_0;
x3_0 = cte.x3_0;
x4_0 = cte.x4_0;
si = cte.si;
FULL_PROBLEM = cte.FULL_PROBLEM;

% Definição dos estados finais
x2f = xf(2);
x3f = xf(3);
x4f = xf(4);

% Computação da eficiência
ef = (x3f*x4f - x3_0*x4_0)/((x4f - x4_0)*si + x4_0*x2_0 - x4f*x2f); 

if FULL_PROBLEM
    if isnan(ef) 
        % Violação da restrição quando ef = NaN
        c = 10;
    else
        c = ef - 0.51;
    end
else
    c = [];
end
ceq = [];

end