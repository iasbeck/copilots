algorithm = 'sqp'; % 'interior-point' / 'sqp' / 'sqp-legacy' / 'active-set'
diagnostics = 'off'; % 'on' / 'off'
displayInfo = 'iter'; % 'iter-detailed' / 'iter' 

maxFunctionEvaluations = inf;
maxIterations = 5000;
optimalityTolerance = 1e-6;
constraintTolerance = 1e-6;
functionTolerance = 1e-6;
stepTolerance = 1e-6;
maxTimeExec = inf; 

plotResults = 1; % 1 / 0
plotTraj = 1; % 1 / 0
plotTrajMidpoints = 1; % 1 / 0 

collocationType = 'trap'; % 'trap' / 'hersim'
