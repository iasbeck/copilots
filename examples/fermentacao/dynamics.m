%#ok<*INUSD>

% Aqui devem ser inseridas as equações diferenciais que descrevem o
% comportamento dinâmico do sistema
function dx = dynamics(x, u, t)

% x - Vetor (nx, 1)
% u - Vetor (nu,1)
% t - Variável temporal

% Importação de constantes globais
global cte
mu0 = cte.mu0;
Kp = cte.Kp;
Ks = cte.Ks;
epsilon0 = cte.epsilon0;
Kp1 = cte.Kp1;
Ks1 = cte.Ks1;
Ysx = cte.Ysx;
si = cte.si;

% Obtenção dos estados e controles
x1 = x(1);
x2 = x(2);
x3 = x(3);
x4 = x(4);

u1 = u(1);

% Computação de variáveis auxiliares
mu = (mu0/(1 + x3/Kp))*(x2/(Ks + x2));
epsilon = (epsilon0/(1 + x3/Kp1))*(x2/(Ks1 + x2));

% Equações que descrevem a dinâmica do sistema
dx1 = x1*mu - u1*(x1/x4);
dx2 = -x1*mu/Ysx + u1*((si - x2)/x4);
dx3 = x1*epsilon - u1*(x3/x4);
dx4 = u1;

dx = [dx1; dx2; dx3; dx4];

end