% Importação de constantes globais
global cte
INITIAL_GUESS = cte.INITIAL_GUESS; 

% Definição dos parâmetros do problema
nx = 4; % Número de estados
nu = 1; % Número de controles
N = 30; % Número de nós utilizado na colocação

t0L = 0; % Limite inferior para o tempo final
t0U = 0; % Limite superior para o tempo inicial

tfL = 1; % Limite inferior para o tempo final
tfU = 100; % Limite superior para o tempo inicial

uL = 0; % Limites inferiores para os controles
uU = 12; % Limites superiores para os controles

xL = [0; 0; 0; 0]; % Limites inferiores para os estados 
xU = [inf; inf; inf; inf]; % Limites superiores para os estados

x0L = [1; 150; 0; 10]; % Limites inferiores para os estados iniciais
x0U = [1; 150; 0; 10]; % Limites superiores para os estados iniciais

xfL = [0; 0; 0; 200]; % Limites inferiores para os estados finais
xfU = [inf; inf; inf; 200]; % Limites superiores para os estados finais

% Palpites iniciais para os estados
if ~INITIAL_GUESS
    X0 = [linspace(1, 1, N);
        linspace(150, 0, N);
        linspace(0, 0, N);
        linspace(10, 200, N)];
else 
    load initialGuess 
end

xNames = {'x1', 'x2', 'x3', 'x4'}; % Nomes atribuídos ao estados
uNames = {'u'}; % Nomes atribuídos aos controles