clc; clear; close all;

% Inicialização de constantes globais
global cte
cte.x1_0 = 1;
cte.x2_0 = 150;
cte.x3_0 = 0;
cte.x4_0 = 10;
cte.mu0 = 0.408;
cte.epsilon0 = 1;
cte.Ysx = 0.1;
cte.si = 150;
cte.Ks = 0.22;
cte.Ks1 = 0.44;
cte.Kp = 16;
cte.Kp1 = 71.5;
cte.INITIAL_GUESS = 0;
cte.FULL_PROBLEM = 1;

% Obtenção da solução
copilots; 

% Armazenamento do palpite inicial
load results/solution
x = solution.x; 
u = solution.u;
t = solution.t;

if ~cte.INITIAL_GUESS
    U0 = u;
    X0 = x;
    T0 = [t(1); t(end)];
    save initialGuess X0 U0 T0
end

% Avaliação da eficiência associada ao resultado 
% Importação de constantes globais
x1_0 = cte.x1_0;
x2_0 = cte.x2_0;
x3_0 = cte.x3_0;
x4_0 = cte.x4_0;
si = cte.si;
x2 = x(2,:);
x3 = x(3,:);
x4 = x(4,:);

% Computação e apresentação da eficiência
ef = (x3.*x4 - x3_0*x4_0)./((x4 - x4_0)*si + x4_0*x2_0 - x4.*x2); 
figure; 
plot(t(2:end), ef(2:end), '.-', 'LineWidth', 1.5, 'MarkerSize', 15);
grid on; axis tight;
legend('ef');