%#ok<*INUSL>

% Aqui deve ser definida a função φ empregada no cálculo do custo 
% J = φ(x(tf)) + ∫ L(x(t), u(t), t)dt
function phi = costPhi(xf, tf)

% xf - Vetor (nx, 1) - Armazerna x(tf)
% tf - Tempo final da simulação

% Obtenção dos estados finais
x3f = xf(3);
x4f = xf(4);

% Computação do custo
phi = -x3f*x4f;

end