%#ok<*INUSD>

% Esta função possibilita a inserção de restrições terminais. Podemos
% definir restrições de igualdade (h = 0) ou de desigualdade (g <= 0).
% Considere que tenhamos várias restrições de igualdade e desigualdade
%
% h1 = 0
% h2 = 0
% h3 = 0
%
% g1 <= 0
% g2 <= 0
% g3 <= 0
%
% Devemos definir c e ceq como
%
% c = [g1; g2; g3]
% ceq = [h1; h2; h3].

function [c, ceq] = boundConstraint(x0, xf, tf)

ceq = [];
c = [];

end