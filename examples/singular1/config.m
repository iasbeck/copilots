nx = 3; % Número de estados
nu = 1; % Número de controles
N = 31; % Número de nós utilizado na colocação

t0L = 0; % Limite inferior para o tempo final
t0U = 0; % Limite superior para o tempo inicial

tfL = 5; % Limite inferior para o tempo final
tfU = 5; % Limite superior para o tempo inicial

uL = -1; % Limites inferiores para os controles
uU = 1; % Limites superiores para os controles

xL = [-inf; -inf; -inf]; % Limites inferiores para os estados 
xU = [inf; inf; inf]; % Limites superiores para os estados

x0L = [0; 1; 0]; % Limites inferiores para os estados iniciais
x0U = [0; 1; 0]; % Limites superiores para os estados iniciais

xfL = [-inf; -inf; -inf]; % Limites inferiores para os estados finais
xfU = [inf; inf; inf]; % Limites superiores para os estados finais

U0 = 0.1*ones(1, N); % Palpites iniciais para os controles
X0 = [linspace(0, 0, N); % Palpites iniciais para os estados
      linspace(1, 1, N);
      linspace(0, 0, N)]; 
T0 = [0, 5]; % Palpite inicial para os tempos inicial e final

xNames = {'x1', 'x2', 'x3'}; % Nomes atribuídos ao estados
uNames = {'u'};  % Nomes atribuídos aos controles