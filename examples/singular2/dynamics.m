%#ok<*INUSD>

% Aqui devem ser inseridas as equações diferenciais que descrevem o
% comportamento dinâmico do sistema
function dx = dynamics(x, u, t)

% x - Vetor (nx, 1)
% u - Vetor (nu,1)
% t - Variável temporal

% Obtenção dos estados e controles
x1 = x(1);
x2 = x(2);
u1 = u(1);

% Definição das equações que descrevem a dinâmica do sistema
dx1 = x2;
dx2 = u1;
dx3 = x1^2 + x2^2;

dx = [dx1; dx2; dx3];

end