nx = 2; % Número de estados
nu = 1; % Número de controles
N = 50; % Número de nós utilizado na colocação

t0L = 0; % Limite inferior para o tempo final
t0U = 0; % Limite superior para o tempo inicial

tfL = 1; % Limite inferior para o tempo final
tfU = 1; % Limite superior para o tempo inicial

uL = -inf; % Limites inferiores para os controles
uU = inf; % Limites superiores para os controles

xL = [-inf; -inf]; % Limites inferiores para os estados 
xU = [inf, inf]; % Limites superiores para os estados

x0L = [1; 1]; % Limites inferiores para os estados iniciais
x0U = [1; 1]; % Limites superiores para os estados iniciais

xfL = [0; 0]; % Limites inferiores para os estados finais
xfU = [0; 0]; % Limites superiores para os estados finaissss

U0 = zeros(1, N); % Palpites iniciais para os controles
X0 = [linspace(1, 0, N); % Palpites iniciais para os estados
      linspace(1, 0, N)]; 
T0 = [0; 1]; % Palpite inicial para os tempos inicial e final

xNames = {'x1'; 'x2'}; % Nomes atribuídos ao estados
uNames = {'u'}; % Nomes atribuídos aos controles