%#ok<*INUSD>

% Aqui deve ser definida a função φ empregada no cálculo do custo 
% J = φ(x(tf)) + ∫ L(x(t), u(t), t)dt
function phi = costPhi(xf, tf)

% xf - Vetor (nx, 1) - Armazerna x(tf)
% tf - Tempo final da simulação

phi = 0;

end