%#ok<*INUSL>

% Aqui deve ser definido o integrando L empregado no cálculo do custo 
% J = φ(x(tf)) + ∫ L(x(t), u(t), t)dt
function L = costL(x, u, t) 

% x - Vetor (nx, 1)
% u - Vetor (nu, 1)
% t - Variável temporal

L = u^2;

end
