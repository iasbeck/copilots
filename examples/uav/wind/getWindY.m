function vy = getWindY(x1, x2)

x = x1;
y = x2;

X = 0:20;
Y = 0:10;
Vy = loadWindY();

[Xm, Ym] = meshgrid(X, Y);

vy = interp2(Xm, Ym, Vy, x, y);

end

