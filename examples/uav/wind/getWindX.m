function vx = getWindX(x1, x2)

x = x1;
y = x2;

X = 0:20;
Y = 0:10;
Vx = loadWindX();

[Xm, Ym] = meshgrid(X, Y);
vx = interp2(Xm, Ym, Vx, x, y);

end

