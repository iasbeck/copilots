%#ok<*INUSD>

% Aqui devem ser inseridas as equações diferenciais que descrevem o
% comportamento dinâmico do sistema
function dx = dynamics(x, u, t)

% x - Vetor (nx, 1)
% u - Vetor (nu,1)
% t - Variável temporal

% Importação de constantes globais
global cte
g = cte.g;
c = cte.c;
m = cte.m;
WIND_ENABLE = cte.WIND_ENABLE;

% Definição dos estados e controles
x1 = x(1);
x2 = x(2);
x3 = x(3);
x4 = x(4);

u1 = u(1);
u2 = u(2);

% Verificação da presença ou não de vento
if WIND_ENABLE
    vx = getWindX(x1, x2);
    vy = getWindY(x1, x2);
else
    vx = 0; 
    vy = 0;
end

% Definição das equações que descrevem a dinâmica do sistema
dx1 = x3;
dx2 = x4;
dx3 = g*tan(u1)*cos(u2) - (c/m)*(x3 - vx);
dx4 = g*tan(u1)*sin(u2) - (c/m)*(x4 - vy);

dx = [dx1; dx2; dx3; dx4];

end