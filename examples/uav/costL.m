%#ok<*INUSL>

% Aqui deve ser definido o integrando L empregado no cálculo do custo 
% J = φ(x(tf)) + ∫ L(x(t), u(t), t)dt
function L = costL(x, u, t)

% x - Vetor (nx, 1)
% u - Vetor (nu, 1)
% t - Variável temporal

% Importação de constantes globais
global cte
Ki = cte.Ki;
m = cte.m;
g = cte.g;

% Obtenção dos estados e controles
u1 = u(1);

% Definição do custo
L = Ki*m*g*(1/cos(u1));

end
