%#ok<*INUSD>
 %#ok<*INUSL>

% Esta função possibilita a inserção de restrições terminais. Podemos
% definir restrições de igualdade (h = 0) ou de desigualdade (g <= 0).
% Considere que tenhamos várias restrições de igualdade e desigualdade
%
% h1 = 0
% h2 = 0
% h3 = 0
%
% g1 <= 0
% g2 <= 0
% g3 <= 0
%
% Devemos definir c e ceq como
%
% c = [g1; g2; g3]
% ceq = [h1; h2; h3].

function [c, ceq] = boundConstraint(x0, xf, tf)

% Importação de constantes globais
global cte
rx = cte.rx;
ry = cte.ry;
r = cte.r; 

% Importação dos estados finais
x1f = xf(1);
x2f = xf(2);

% Definição da restrição de contorno
% (x1f - rx)^2 + (x2f - ry)^2 <= r
dr = (x1f - rx)*(x1f - rx) + (x2f - ry)*(x2f - ry);

ceq = [];
c = dr - r;

end