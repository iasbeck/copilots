clc; clear; close all; 

% Importação das funções auxiliares
addpath('wind')

% Definição de constantes globais
global cte
cte.phi_max = pi/6;
cte.px_max = 20;
cte.py_max = 10;
cte.rx = 15;
cte.ry = 5;
cte.r = 1; 
cte.Ki = 5.08;
cte.m = 0.5;
cte.g = 9.81;
cte.c = 0.2;
cte.WIND_ENABLE = 1;
cte.INITIAL_GUESS = 1;

% Obtenção da solução
copilots;

% Armazenamento da solução para posterior utilização como palpite inicial
if ~cte.INITIAL_GUESS
    load('results/solution.mat');
    X0 = solution.x;
    U0 = solution.u;
    t0 = solution.t(1);
    tF = solution.t(end);
    T0 = [t0; tF];
    save initialGuess X0 U0 T0
end