% Importação de constantes globais
global cte
phi_max = cte.phi_max;
px_max = cte.px_max;
py_max = cte.py_max;
rx = cte.rx;
ry = cte.ry;
INITIAL_GUESS = cte.INITIAL_GUESS;

% Definição dos parâmetros do problema
nx = 4; % Número de estados
nu = 2; % Número de controles
N = 10; % Número de nós utilizado na colocação

t0L = 0; % Limite inferior para o tempo final
t0U = 0; % Limite superior para o tempo inicial

tfL = 0.5; % Limite inferior para o tempo final
tfU = 10; % Limite superior para o tempo inicial

uL = [0; -pi]; % Limites inferiores para os controles
uU = [phi_max; pi]; % Limites superiores para os controles

xL = [0; 0; -inf; -inf]; % Limites inferiores para os estados 
xU = [px_max; py_max; inf; inf]; % Limites superiores para os estados

x0L = [1; 5; 0; 0]; % Limites inferiores para os estados iniciais
x0U = [1; 5; 0; 0]; % Limites superiores para os estados iniciais

xfL = [0; 0; -inf; -inf]; % Limites inferiores para os estados finais
xfU = [px_max; py_max; inf; inf]; % Limites superiores para os estados finais

if INITIAL_GUESS
    load initialGuess
end

xNames = {'x1', 'x2', 'x3', 'x4'}; % Nomes atribuídos ao estados
uNames = {'u1', 'u2'};  % Nomes atribuídos aos controles